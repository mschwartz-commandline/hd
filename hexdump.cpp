#include "hd.h"

/**
 * Dump Bytes
 */
void HexDump(unsigned char *ptr, int length, int width) {
  printf("width %d\n", width);
  unsigned long addr = 0;
  int count = 0;
  char hex[3 * width + 1];
  char ascii[width + 1];
  while (length > 0) {
    memset(hex, ' ', width * 3);
    hex[width * 3] = '\0';
    memset(ascii, '.', width * sizeof(unsigned char));
    ascii[width * sizeof(unsigned char)] = '\0';
    printf("[%08x] ", addr);

    for (int i = 0; i < width && --length > 0; i++) {
      ascii[i] = isprint(*ptr) ? *ptr : '.';
      char work[4];
      sprintf(work, "%02x", *ptr++);
      strncpy(&hex[3 * i], work, 2);
      count++;
      if (count > width - 1) {
        count = 0;
        addr += width;
        break;
      }
    }

    if (options.ascii) {
      printf("%s *%s*\n", hex, ascii);
    }
    else {
      printf("%s\n", hex);
    }
  }
}

/**
 * Dump Words
 */
void HexDump(unsigned short *ptr, int length, int width) {
  unsigned long addr = 0;
  int count = 0;
  int i;
  char *p = (char *)&ptr[0];
  char ascii[width * sizeof(unsigned short) + 1];
  char hex[width * 5 + 1];
  const unsigned char *start = (unsigned char *)ptr;

  while (length > 0) {
    if ((unsigned char *)ptr >= &start[length]) {
      break;
    }

    memset(hex, ' ', width * 5);
    hex[width * 5] = '\0';

    memset(ascii, '.', width * sizeof(unsigned short));
    ascii[width * sizeof(unsigned short)] = '\0';

    printf("[%08x] ", addr);

    for (i = 0; i < width && --length > 0; i++) {
      if ((unsigned char *)ptr >= &start[length]) {
        break;
      }
      char work[6];
      sprintf(work, "%04x", *ptr++);
      strncpy(&hex[i * 5], work, 4);
      ascii[i * 2] = isprint(*p) ? *p : '.';
      p++;
      ascii[i * 2 + 1] = isprint(*p) ? *p : '.';
      p++;

      count++;
      if (count > width - 1) {
        count = 0;
        addr += width * 2;
        break;
      }
    }

    if (options.ascii) {
      printf("%s *%s*\n", hex, ascii);
    }
    else {
      printf("%s\n", hex);
    }
  }
}

/**
 * Dump Longs
 */
void HexDump(unsigned long *ptr, int length, int width) {
  printf("width %d\n", width);
  unsigned long addr = 0;
  int count = 0;
  int i;
  char *p = (char *)&ptr[0];

  char ascii[width * sizeof(unsigned long) + 1];
  char hex[width * 9 + 1];

  const unsigned char *start = (unsigned char *)ptr;

  while (length > 0) {
    if ((unsigned char *)ptr >= &start[length]) {
      break;
    }

    memset(hex, ' ', width * 9);
    hex[width * 9] = '\0';

    memset(ascii, '.', width * sizeof(unsigned long));
    ascii[width * sizeof(unsigned long)] = '\0';

    printf("[%08x] ", addr);

    for (i = 0; i < width && --length > 0; i++) {
      if ((unsigned char *)ptr >= &start[length]) {
        break;
      }
      char work[9];
      sprintf(work, "%08x", *ptr++);
      strncpy(&hex[i * 9], work, 8);
      ascii[i * 8] = isprint(*p) ? *p : '.';
      p++;
      ascii[i * 8 + 1] = isprint(*p) ? *p : '.';
      p++;
      ascii[i * 8 + 2] = isprint(*p) ? *p : '.';
      p++;
      ascii[i * 8 + 3] = isprint(*p) ? *p : '.';
      p++;
      ascii[i * 8 + 4] = isprint(*p) ? *p : '.';
      p++;
      ascii[i * 8 + 5] = isprint(*p) ? *p : '.';
      p++;
      ascii[i * 8 + 6] = isprint(*p) ? *p : '.';
      p++;
      ascii[i * 8 + 7] = isprint(*p) ? *p : '.';
      p++;

      count++;
      if (count > width - 1) {
        count = 0;
        addr += width * 2;
        break;
      }
    }
    if (options.ascii) {
      printf("%s *%s*\n", hex, ascii);
    }
    else {
      printf("%s\n", hex);
    }
  }
}
