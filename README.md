# hd

Simple but useful dump file in hex to screen

This isn't the only hex dump program out there, but I wanted a version that does what I want by default.  I don't want
to be looking at man pages to figure out what switches I really want, or creating shell aliases.

```
Usage:
        ./hd [options] filename

  Dumps the contents of the file named filename to the screen in hex

        -b - print the contents as bytes (the default)
        -w - print the contents as words
        -l - print the contents as longs

  Additional options
        -n count - prints count values on a line
	-x - disable ascii printout

```

**## Examples:

```
$ hd -b -n 8 main.o

00000000 7f 45 4c 46 02 01 01 00    *........*
00000008 00 00 00 00 00 00 00 00    *..>.....*
00000010 01 00 3e 00 01 00 00 00    *........*
00000018 00 00 00 00 00 00 00 00    *........*
00000020 00 00 00 00 00 00 00 00    *........*
00000028 b0 18 00 00 00 00 00 00    *....@...*
00000030 00 00 00 00 40 00 00 00    *..@.....*
00000038 00 00 40 00 12 00 11 00    *........*
00000040 01 00 00 00 06 00 00 00    *UH..H...*
00000048 55 48 89 e5 48 83 ec 10    *H.}.H.=.*
00000050 48 89 7d f8 48 8d 3d 00    *........*
00000058 00 00 00 e8 00 00 00 00    *H.E.H..H*
00000060 48 8b 45 f8 48 89 c6 48    *.=......*
00000068 8d 3d 00 00 00 00 b8 00    *........*
00000070 00 00 00 e8 00 00 00 00    *H.=.....*
00000078 48 8d 3d 00 00 00 00 e8    *....H.=.*
00000080 00 00 00 00 48 8d 3d 00    *........*
00000088 00 00 00 e8 00 00 00 00    *H.=.....*
00000090 48 8d 3d 00 00 00 00 e8    *....H.=.*
00000098 00 00 00 00 48 8d 3d 00    *........*
000000a0 00 00 00 e8 00 00 00 00    *H.=.....*
000000a8 48 8d 3d 00 00 00 00 e8    *....H.=.*
000000b0 00 00 00 00 48 8d 3d 00    *........*
000000b8 00 00 00 e8 00 00 00 00    *H.=.....*
000000c0 48 8d 3d 00 00 00 00 e8    *....H.=.*
000000c8 00 00 00 00 48 8d 3d 00    *........*
000000d0 00 00 00 e8 00 00 00 00    *.......U*
000000d8 b8 01 00 00 00 c9 c3 55    *H..H.. H*
000000e0 48 89 e5 48 83 ec 20 48    *.}..u..U*
000000e8 89 7d e8 89 75 e4 89 55    *.H.=....*
000000f0 e0 48 8d 3d 00 00 00 00    *........*
000000f8 b8 00 00 00 00 e8 00 00    *...E....*
00000100 00 00 c7 45 fc 00 00 00    *..E.;E.}*
00000108 00 8b 45 fc 3b 45 e0 7d    *..m...}.*
00000110 11 83 6d e4 01 83 7d e4    *.~......*
...
```

```
$ hd -b -n 16 main.o
00000000 7f 45 4c 46 02 01 01 00 00 00 00 00 00 00 00 00    *..>.............*
00000010 01 00 3e 00 01 00 00 00 00 00 00 00 00 00 00 00    *................*
00000020 00 00 00 00 00 00 00 00 b8 18 00 00 00 00 00 00    *....@.....@.....*
00000030 00 00 00 00 40 00 00 00 00 00 40 00 12 00 11 00    *........UH..H...*
00000040 01 00 00 00 06 00 00 00 55 48 89 e5 48 83 ec 10    *H.}.H.=.........*
00000050 48 89 7d f8 48 8d 3d 00 00 00 00 e8 00 00 00 00    *H.E.H..H.=......*
00000060 48 8b 45 f8 48 89 c6 48 8d 3d 00 00 00 00 b8 00    *........H.=.....*
00000070 00 00 00 e8 00 00 00 00 48 8d 3d 00 00 00 00 e8    *....H.=.........*
00000080 00 00 00 00 48 8d 3d 00 00 00 00 e8 00 00 00 00    *H.=.........H.=.*
00000090 48 8d 3d 00 00 00 00 e8 00 00 00 00 48 8d 3d 00    *........H.=.....*
000000a0 00 00 00 e8 00 00 00 00 48 8d 3d 00 00 00 00 e8    *....H.=.........*
000000b0 00 00 00 00 48 8d 3d 00 00 00 00 e8 00 00 00 00    *H.=.........H.=.*
000000c0 48 8d 3d 00 00 00 00 e8 00 00 00 00 48 8d 3d 00    *...............U*
000000d0 00 00 00 e8 00 00 00 00 b8 01 00 00 00 c9 c3 55    *H..H.. H.}..u..U*
000000e0 48 89 e5 48 83 ec 20 48 89 7d e8 89 75 e4 89 55    *.H.=............*
...
```

```
$ hd -w -n 16 main.o 
00000000 457f 464c 0102 0001 0000 0000 0000 0000 0001 003e 0001 0000 0000 0000 0000 0000    *................*
00000020 0000 0000 0000 0000 18b8 0000 0000 0000 0000 0000 0040 0000 0000 0040 0012 0011    *........UH..H...*
00000040 0001 0000 0006 0000 4855 e589 8348 10ec 8948 f87d 8d48 003d 0000 e800 0000 0000    *H.E.H..H.=......*
00000060 8b48 f845 8948 48c6 3d8d 0000 0000 00b8 0000 e800 0000 0000 8d48 003d 0000 e800    *....H.=.........*
00000080 0000 0000 8d48 003d 0000 e800 0000 0000 8d48 003d 0000 e800 0000 0000 8d48 003d    *........H.=.....*
000000a0 0000 e800 0000 0000 8d48 003d 0000 e800 0000 0000 8d48 003d 0000 e800 0000 0000    *H.=.........H.=.*
000000c0 8d48 003d 0000 e800 0000 0000 8d48 003d 0000 e800 0000 0000 01b8 0000 c900 55c3    *H..H.. H.}..u..U*
000000e0 8948 48e5 ec83 4820 7d89 89e8 e475 5589 48e0 3d8d 0000 0000 00b8 0000 e800 0000    *...E......E.;E.}*
00000100 0000 45c7 00fc 0000 8b00 fc45 453b 7de0 8311 e46d 8301 e47d 7e00 b807 0001 0000    *.........t5H.E.H*
00000120 05eb 00b8 0000 8400 74c0 4835 458b 48e8 508d 4801 5589 0fe8 00b6 4588 80fb fb7d    *.v...E...y..E...*
00000140 761f 0f08 45b6 84fb 79c0 c604 fb45 0f2e 45b6 89fb e8c7 0000 0000 4583 01fc a9eb    *.*...........UH.*
```


