#include "hd.h"

int main(int ac, char *av[]) {

  if (ac < 2) {
    printf("%s: Not enough arguments\n\n", av[0]);
    return help(av[0]);
  }

  //  while ((opt = getopt(ac, av, "nt:")) != -1) {
  int opt;
  bool countFlag = false;
  while ((opt = getopt(ac, av, opts)) != -1) {
    //    printf("opt (%d) %c\n", opt, opt);
    switch (opt) {
      case 'b':
        options.format = BYTES;
        break;
      case 'w':
        options.format = WORDS;
        break;
      case 'l':
        options.format = LONGS;
        if (!countFlag) {
          options.count = 8;
        }
        break;
      case 'h':
        return help(av[0]);
      case 'n':
        countFlag = true;
        options.count = atoi(optarg);
        break;
      case 'x':
        options.ascii = false;
        break;
      default:
        printf("\n");
        //        printf("Invalid option %c\n", opt);
        return help(av[0]);
    }
  }

  for (; optind < ac; optind++) {
    options.filename = strdup(av[optind]);
    break;
  }

  if (options.filename == NULL) {
    printf("%s: filename argument required\n\n", av[0]);
    return help(av[0]);
  }

  // read file into memory
  // this might be better with mmap
  int fd = open(options.filename, O_RDONLY);
  if (fd < 0) {
    perror("open");
    printf("open %s failed\n", options.filename);
    return 1;
  }
  ssize_t size = lseek(fd, 0, 2);
  lseek(fd, 0, 0);
  unsigned char *data =  (unsigned char *)malloc(size);
  read(fd, data, size);
  close(fd);

  // call the proper overloaded HexDump method
  int sz = int(size);
  switch (options.format) {
    case BYTES:
      HexDump(data, sz, options.count);
      break;
    case WORDS:
      HexDump((unsigned short *)data, sz, options.count);
      break;
    case LONGS:
      HexDump((unsigned long *)data, sz, options.count);
      break;
  }

  return 0;
}
