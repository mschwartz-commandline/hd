#ifndef HD_H
#define HD_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <ctype.h>

#ifndef NULL
#define NULL (0)
#endif

extern void HexDump(unsigned char *ptr, int length, int width = 8);
extern void HexDump(unsigned short *ptr, int length, int width = 8);
extern void HexDump(unsigned long *ptr, int length, int width = 8);

#include "options.h"

#endif
