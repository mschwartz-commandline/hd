#include "hd.h"

OPTIONS options;

int help(char *name) {
  printf("  Usage:\n");
  printf("\t%s [options] filename\n", name);
  printf("\n  Dumps the contents of the file named filename to the screen in "
         "hex\n");
  printf("\n  Options (only one of these allowed)\n");
  printf("\t-b - print the contents as bytes (the default)\n");
  printf("\t-w - print the contents as words\n");
  printf("\t-l - print the contents as longs\n");
  printf("\n  Additional options\n");
  printf("\t-n count - prints count values on a line\n");
  printf("\t-x - disable ascii printout\n");
  printf("\n\n");
  return 1;
}
