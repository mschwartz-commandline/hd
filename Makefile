OBJ=	main.o hexdump.o options.o

.cpp.o:
	gcc -c -o $*.o $*.cpp

hd:	$(OBJ)
	gcc -o hd $(OBJ)

install: hd
	install hd /usr/local/bin

clean:
	rm -f hd $(OBJ)
