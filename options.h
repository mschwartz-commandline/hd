#ifndef OPTIONS_H
#define OPTIONS_H

#include "hd.h"
enum FORMAT { BYTES, WORDS, LONGS };

struct OPTIONS {
  FORMAT format;
  int count;
  char *filename;
  bool ascii;
  OPTIONS() {
    format = BYTES;
    count = 16;
    filename = NULL;
    ascii = true;
  }
  void Dump() {
    printf("format: %d\n", format);
    printf("count: %d\n", count);
    printf("filename: %s\n", filename);
  }
};

static const char *opts = "bwlhn:x";

extern OPTIONS options;

extern int help(char *arg);

#endif
